const fs = require("fs");

const mergeLocales = async () => {
  // Define the path to the features directory
  const featuresDir = "./src/features/";

  // Read the root locale files
  const rootLocaleEn = require("./src/core/locales/en.json");
  const rootLocaleAr = require("./src/core/locales/ar.json");

  // Read the features directory
  const features = fs.readdirSync(featuresDir);
  for (const feature of features) {
    const featureDir = featuresDir + feature + "/";
    const localeDir = featureDir + "locales/";

    // Read the locale files for the feature
    const featureLocaleEn = require(`./${localeDir}en.json`);
    const featureLocaleAr = require(`./${localeDir}ar.json`);

    // Merge the locale files with the root locale files
    Object.assign(rootLocaleEn, featureLocaleEn);
    Object.assign(rootLocaleAr, featureLocaleAr);
  }

  // Save the merged locale files back to disk
  fs.writeFileSync(
    "./src/core/locales/en.json",
    JSON.stringify(rootLocaleEn, null, 2)
  );
  fs.writeFileSync(
    "./src/core/locales/ar.json",
    JSON.stringify(rootLocaleAr, null, 2)
  );
};

mergeLocales();
