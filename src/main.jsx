import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import { store } from "@/core/app/store";
import { persistStore } from "redux-persist";
import { APPLoader } from "@/core/components";
import "./index.css";

const App = React.lazy(() => import("@/core/App.jsx"));

export const persistor = persistStore(store);

ReactDOM.createRoot(document.getElementById("root")).render(
  <BrowserRouter>
    <Provider store={store}>
      <PersistGate loading={<div>initializing</div>} persistor={persistor}>
        <React.Suspense fallback={<APPLoader />}>
          <App />
        </React.Suspense>
      </PersistGate>
    </Provider>
  </BrowserRouter>
);
