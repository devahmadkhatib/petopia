import { AppBar, Box, IconButton, Typography, styled } from "@mui/material";

export const Nav = styled(AppBar)(({ theme }) => ({
    width: '100%',
    height: '42px',
    background: theme.palette.primary.main,
    boxShadow: 'none',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'static',
    [theme.breakpoints.down('sm')]: {
    height:'auto'
    },
}))
export const NavContainer = styled(Box)(({ theme }) => ({
    width: '80%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
        width: '90%',
        padding: '12px 0 '
    }
}))
export const NavContainerIcon = styled(Box)(({ theme }) => ({
    display: 'flex',
    gap: '8px',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
        gap: '0px',
    }
}))
export const NavContainerContact = styled(Box)(({ theme }) => ({
    display: 'flex',
    gap: '42px',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
        gap: '16px',
    },
    [theme.breakpoints.down('sm')]: {
        flexDirection: 'column',
        gap: '16px',
    }
}))
export const NavIconButton = styled(IconButton)(({ theme }) => ({
    color: theme.palette.primary.light,
}))
export const TextContact = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.light,
    fontSize: '16px',
    fontStyle: 'normal',
    fontWeight: 700,
    lineHeight: '19px',
}))
export const InfoContact = styled(Box)(({ theme }) => ({
    display: 'flex',
    gap: '4px',
    alignItems: 'center',
    color: theme.palette.primary.light,
    textDecoration:'none',
    [theme.breakpoints.down('sm')]:{
        flexDirection:'column'
    }
}))