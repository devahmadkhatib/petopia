import React from 'react'
import * as Style from './StyleNavbar';
import * as Icons from '@/core/config/import/icons';
import { Link } from 'react-router-dom';
const Navbar = () => {
    return (
        <Style.Nav>
            <Style.NavContainer>
                <Style.NavContainerIcon  >
                    <Style.NavIconButton>
                        <Icons.FacebookIcon style={{ width: '20px' }} />
                    </Style.NavIconButton>
                    <Style.NavIconButton>
                        <Icons.InstagramIcon style={{ width: '20px' }} />
                    </Style.NavIconButton>
                    <Style.NavIconButton>
                        <Icons.TwitterIcon style={{ width: '20px' }} />
                    </Style.NavIconButton>
                    <Style.NavIconButton>
                        <Icons.WhatsAppIcon style={{ width: '20px' }} />
                    </Style.NavIconButton>
                </Style.NavContainerIcon>
                <Style.NavContainerContact>
                    <Style.InfoContact component={Link}>
                        <Icons.CallIcon style={{ width: '20px' }} />
                        <Style.TextContact>
                            0000 - 123456789
                        </Style.TextContact>
                    </Style.InfoContact>
                    <Style.InfoContact component={Link}>
                        <Icons.EmailIcon style={{ width: '20px' }} />
                        <Style.TextContact>
                            info@example.com
                        </Style.TextContact>
                    </Style.InfoContact>
                 
                </Style.NavContainerContact>
            </Style.NavContainer>
        </Style.Nav>
    )
}

export default Navbar