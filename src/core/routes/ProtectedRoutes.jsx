import React from "react";
import { useSelector } from "react-redux";
import { Outlet } from "react-router-dom";
import * as selectors from "@/core/config/import/selectors";

function ProtectedRoutes() {
  const token = useSelector(selectors.userToken);
  return token ? <Outlet /> : <></>;
}

export default ProtectedRoutes;
