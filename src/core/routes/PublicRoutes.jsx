import React from "react";
import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";
import * as selectors from "@/core/config/import/selectors";

function PublicRoutes() {
  const token = useSelector(selectors.userToken);
  return token ? <Navigate to="/" /> : <Outlet />;
}

export default PublicRoutes;
