export { default as APPLoader } from "./loaders/AppLoader";

//////////////////////////// formik //////////////////////
// export { default as Form } from "./formik/formikContainer/FormikContainer";
// export { default as Field } from "./formik/formikControl/FormikControl";

////////////////////////// buttons ////////////////////////
export { default as Button } from "./buttons";

////////////////////////// textField ////////////////////////
export { default as InputText } from "./inputText";
////////////////////////// Rate ////////////////////////
export { default as RatingComponent } from "./Rate";
