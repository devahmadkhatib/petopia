import { TextField, styled } from "@mui/material";

export const TextFail = styled(TextField)(({ theme }) => (props) => ({
    background: props.background ? props.background : 'Transparent ',
    width: props.width ? props.width : '100%',
    "& .MuiInputBase-root ": {
        borderRadius: props.borderradius ? props.borderradius : theme.shape.borderRadius,
        height: props.height ? props.height : '52px',
        width: props.width ? props.width : '100%',
        fontWeight: props.fontweight ? props.fontweight : '400',
        color: props.color ? props.color : theme.palette.input.placeholder,
        padding: '0',
    },
    "& .MuiOutlinedInput-notchedOutline": {
        border: props.border ? props.border : 'none',
        borderRadius: props.borderradius ? props.borderradius : theme.shape.borderRadius,

    },
    "& .Mui-focused": {
        "& .MuiOutlinedInput-notchedOutline": {
            border: props.border ? props.border : 'none',
            borderRadius: props.borderradius ? props.borderradius : theme.shape.borderRadius,
            outline: 'none'
        }
    },
    "& .MuiInputBase-input": {
        color: props.color ? props.color : theme.palette.input.placeholder,
        fontSize: props.fontsize ? props.fontsize : '16px',
        fontWeight: props.fontwight ? props.fontwight : '400',
        borderRadius: props.borderradius ? props.borderradius : theme.shape.borderRadius,

    },
    "& .muirtl-1h9uykw-MuiInputBase-input-MuiOutlinedInput-input:autofill": {
        boxShadow: 'none'
    },
}))
