import { styled } from "@mui/material";

export const Content = styled('div')(({ theme }) => (props) => ({
    width: '100%',
    height: '90vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
}))