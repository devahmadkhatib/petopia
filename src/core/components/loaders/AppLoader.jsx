import React from 'react'
import * as Styled from './styled';
import { ThreeDots } from 'react-loader-spinner'
import { motion } from "framer-motion"
import { LogoMAin } from '@/core/assets/images';
function AppLoader() {
  return (
    <Styled.Content> <motion.img src={LogoMAin} style={{ width: '80px' }} alt=""
      initial={{ opacity: 0.5, scale: 0.8 }}
      animate={{ opacity: 1, scale: [0.8, 1, 0.8, 1, 0.8] }}
      transition={{
        duration: 0.2,
        delay: 0.3,
      }}
    />
      <ThreeDots
        height="40"
        width="40"
        radius="9"
        color="#7C58D3"
        ariaLabel="three-dots-loading"
        wrapperStyle={{}}
        wrapperClassName=""
        visible={true}
      /></Styled.Content>
  )
}

export default AppLoader