import { ButtonBase, styled } from "@mui/material";

export const NormalButton = styled(ButtonBase)(({ theme }) => (props) => ({
    color: props.color ? props.color : theme.palette.primary.main,
    background: props.background ? props.background : 'transparent',
    borderRadius: props.borderradios ? props.borderradius : '8px',
    fontSize: props.fontSize ? props.fontSize : '16px',
    fontWeight: props.fontWeight ? props.fontWeight : 700,
    letterSpacing: '0em',
    textAlign: props.textalign ? props.textalign : 'center',
    height: props.height ? props.height : '48px',
    width: props.width ? props.width : '127px',
    padding: props.padding ? props.padding : '5px 0',
    margin: props.margin,
    border: '1px solid transparent',
    borderColor: props.bordercolor,
    borderWidth: props.borderwidth,
    display: props.display,
    alignItems: props.alignitems,
    justifyContent: props.justifycontent,
    backgroundImage: props.backgroundimage
}));