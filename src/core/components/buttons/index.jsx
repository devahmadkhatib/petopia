import React from 'react'
import { NormalButton } from './Styled'
import { Button } from '@mui/material'

const ButtonLight = (props) => {

    return (
        <NormalButton {...props}>
            {props.text}
        </NormalButton>


    )

}

export default ButtonLight