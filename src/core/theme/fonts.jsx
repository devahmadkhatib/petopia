const MAIN_FONT = 'Tajawal';

export const fonts = {
  fontFamily: MAIN_FONT,
  fontSize: 16,
  h1: {
    fontFamily: MAIN_FONT,
    fontSize: 40,
  },
  h2: {
    fontFamily: MAIN_FONT,
    fontSize: 32,
    fontWeight: 700,
  },
  h3: {
    fontFamily: MAIN_FONT,
    fontSize: 24,
  },
  h4: {
    fontFamily: MAIN_FONT,
    fontSize: 20,
  },
  h5: {
    fontFamily: MAIN_FONT,
    fontSize: 16,
  },
  h6: {
    fontFamily: MAIN_FONT,
    fontSize: 14,
  },
};
