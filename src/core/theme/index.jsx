import { breakpoints } from "./breakpoints";
import { fonts } from "./fonts";
import { darkPalette } from "./palette/dark";
import { lightPalette } from "./palette/light";
import { shape } from "./shape";

export const themeSettings = (mode, dir) => {
  return {
    direction: dir,
    palette: {
      mode: mode,
      ...(mode === "dark" ? { ...darkPalette } : { ...lightPalette }),
    },
    typography: { ...fonts },
    breakpoints: { ...breakpoints },
    shape:{...shape}
  };
};
