import { alpha } from "@mui/material";
import { sharedPalette } from "./shared";

export const lightPalette = {
  primary: {
    main: "#9C5BF5;",
    dark: "#0E081E",
    light: "#ffffff",
  },
  input: {
    gray: "#EEEEEE",
    placeholder: " #989898",
    shadow: {
      normal: `${alpha("#a8a8a8", 0.25)} 0px 2px 5px`,
      focus: `${alpha("#9c9c9c", 0.4)} 0px 2px 5px`,
    },
  },
  secondary: {
    main: "#45D3C8",
  },
  background: {
    background: '#FFFEFD'
  },
  ...sharedPalette,
};
