export const sharedPalette = {
  error: {
    light: "#EB5757",
    main: "#EB5757",
  },

  borders: {
    yellow: "rgba(251, 154, 11, 0.5);",
    blue: "rgba(59, 126, 197, 0.5)",
    dark: "#BDBDBD",
  },
  icons: {
    pink: "#ED9D99",
    gradientPink: "linear-gradient(225deg, #ED9D99 0%, #D14DCD 100%)",
  },
  rectangles: {
    blue: "rgba(59, 126, 197, 0.5)",
    pink: "rgba(209, 77, 205, 0.5)",
    yellow: "rgba(251, 154, 11, 0.5)",
    gray: "#E0E0E0",
  },
  stickyDrawer: {
    yellow: "#FB9A0B",
    pink: "#ED9D99",
    blue: "#3B7EC5",
  },
  buttons: {
    blueGradient: "linear-gradient(266.27deg, #56CCF2 0%, #3B7EC5 100%)",
    pinkGradient: "linear-gradient(265.33deg, #FB9A0B 0%, #D14DCD 100%)",
    yellow: "linear-gradient(269.05deg, #F2C94C -0.02%, #FB9A0B 100.9%)",
  },
  floatingButton: {
    pink: "linear-gradient(225deg, #F2C94C 0%, #D14DCD 100%)",
  },
};
