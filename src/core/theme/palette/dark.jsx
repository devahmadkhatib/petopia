import { sharedPalette } from "./shared";

export const darkPalette = {
  primary: {
    main: "#9C5BF5;",
    dark: "#ffffff",
    light: "#0E081E",
  },
  input: {
    gray: "#EEEEEE",
    placeholder: " #989898",
  },
  secondary: {
    main: " #45D3C8",
  },
  background: {
    default: "#333333",
  },
  ...sharedPalette,
};
