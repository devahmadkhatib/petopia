export const breakpoints = {
  keys: ["xs", "ss", "sm", "mm", "md", "lg", "xl"],
  values: {
    xs: 0,
    ss: 400,
    sm: 600,
    mm: 700,
    md: 950,
    lg: 1200,
    xl: 1920,
  },
};
