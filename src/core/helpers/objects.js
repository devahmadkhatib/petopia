export const objectReverser = (object, step) => {
  let key = 0;
  const values = Object.values(object).reverse();
  const final = {};
  values.forEach((value) => {
    key = key + step;
    final[key] = value;
  });
  return final;
};

const validate = (value, initial) => {
  let newObj = {};
  if (
    value.indexOf(".") > 0 &&
    !(value.substring(0, value.indexOf(".")) in initial)
  ) {
    newObj[value.substring(value.indexOf(".") + 1)] = "";
    initial[value.substring(0, value.indexOf("."))] = newObj;
  } else if (
    value.indexOf(".") > 0 &&
    value.substring(0, value.indexOf(".")) in initial
  ) {
    initial[value.substring(0, value.indexOf("."))][
      value.substring(value.indexOf(".") + 1)
    ] = "";
  } else {
    initial[value] = "";
  }
  return initial;
};

const objectKeys = (arr) => {
  const keys = [];
  arr.forEach((object) => {
    for (const [key, val] of Object.entries(object)) {
      if (key === "name") keys.push(val);
    }
  });
  return keys;
};

export const extractInitialValues = (values, extra) => {
  let initialValues = {};
  objectKeys(values).forEach((val) => {
    validate(val, initialValues);
  });
  if (extra) initialValues = { ...initialValues, ...extra };
  return initialValues;
};
