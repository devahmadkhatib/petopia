import {
  isPending,
  isFulfilled,
  isRejected,
  isRejectedWithValue,
} from "@reduxjs/toolkit";
import * as actions from "@/core/config/import/actions";
import { useAppDispatch } from "../store";

const excludedEndpoints = [];

export const globalLoader = (store) => (next) => (action) => {
  if (
    isPending(action) &&
    !excludedEndpoints.includes(action.meta.arg.endpointName)
  ) {
    useAppDispatch(actions.setLoading(true));
  } else if (
    isFulfilled(action) ||
    isRejected(action) ||
    isRejectedWithValue(action)
  ) {
    useAppDispatch(actions.setLoading(false));
  }

  return next(action);
};
