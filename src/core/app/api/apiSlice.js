import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const baseQuery = fetchBaseQuery({
  baseUrl: import.meta.env.VITE_BASE_URL,
  prepareHeaders: (headers, { getState }) => {
    const { token } = getState().credentials;
    if (token) headers.set("authorization", `${token}`);
    return headers;
  },
});

export const apiSlice = createApi({
  baseQuery: baseQuery,
  refetchOnReconnect: true,
  endpoints: (builder) => ({}),
});
