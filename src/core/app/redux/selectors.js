export const isLoading = (state) => state.app.loading;
export const appDirection = (state) => state.cookies.dir;
export const appLanguage = (state) => state.cookies.lang;
export const appMode = (state) => state.cookies.mode;
