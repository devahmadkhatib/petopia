import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  mode: "light",
  lang: "en",
  dir: "ltr",
};

const cookiesSlice = createSlice({
  name: "cookies",
  initialState: initialState,
  reducers: {
    setMode: (state, action) => {
      let mode = state.mode;
      if (mode === "light") state.mode = "dark";
      else state.mode = "light";
    },
    changeLanguage: (state, action) => {
      state.lang = action.payload;
      if (action.payload === "ar") state.dir = "rtl";
      else state.dir = "ltr";
    },
  },
});

export const { setMode, changeLanguage } = cookiesSlice.actions;
export default cookiesSlice.reducer;
