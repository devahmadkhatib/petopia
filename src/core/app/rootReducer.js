import { persistReducer } from "redux-persist";
import { apiSlice } from "./api/apiSlice";
import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";

import appSlice from "@/core/app/redux/slices/appSlice";
import cookiesSlice from "@/core/app/redux/slices/cookiesSlice";
import credentialsSlice from "@/features/user/redux/slices/credentialsSlice";

const persistConfig = {
  key: "root",
  version: 1,
  storage,
  whitelist: ["credentials", "cookies"],
};

let reducer = combineReducers({
  [apiSlice.reducerPath]: apiSlice.reducer,
  app: appSlice,
  cookies: cookiesSlice,
  credentials: credentialsSlice,
});

export const rootReducer = persistReducer(persistConfig, reducer);
