import React from 'react'
import * as Comp from '@/features/Home/components';
import { Container } from './style';
const Intro = () => {
  return (
    <Container>
      <Comp.Header />
      <Comp.Banaras />
      <Comp.Service />
      <Comp.Blog />
      <Comp.ContactUs />
      <Comp.OtherPosts />
    </Container>
  )
}

export default Intro