import { Box, styled } from "@mui/material";

export const Container = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '48px'
}));
