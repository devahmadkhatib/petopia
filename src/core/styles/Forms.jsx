import { Stack } from "@mui/material";
import { styled } from "@mui/material/styles";

export const GeneralForm = styled(Stack)(({ theme }) => ({
  flexDirection: "column",
  gap: 16,
  "& .actions": {
    marginTop: 16,
  },
  "& .sub-actions": {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 5,
  },
}));
