import { Box, Stack } from "@mui/material";
import { styled } from "@mui/material/styles";

export const InfoHolder = styled(Stack)(({ theme }) => ({
  position: "relative",
  // height: "100vh",
  minWidth: "100%",
  overflowY: "auto",
  flexDirection: "row",
}));
