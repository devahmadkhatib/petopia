import ApiProvider from "./api-provider";
import HttpMethod from "./http-methods";

class ApiService {
  constructor(config) {
    this.provider = new ApiProvider(config);
  }

  async get(url, config) {
    const method = HttpMethod.GET;
    const response = await this.provider.request({ method, url, ...config });
    return response;
  }

  async delete(url, config) {
    const method = HttpMethod.DELETE;
    const response = await this.provider.request({ method, url, ...config });
    return response;
  }

  async post(url, data, config) {
    const method = HttpMethod.POST;
    const response = await this.provider.request({
      method,
      url,
      data,
      ...config,
    });
    return response;
  }

  async put(url, data, config) {
    const method = HttpMethod.PUT;
    const response = await this.provider.request({
      method,
      url,
      data,
      ...config,
    });
    return response;
  }

  async patch(url, data, config) {
    const method = HttpMethod.PATCH;
    const response = await this.provider.request({
      method,
      url,
      data,
      ...config,
    });
    return response.data;
  }
}

export default ApiService;
