import axios from "axios";
import { remoteServiceBaseApiUrl } from "@/core/config/static/constants";
import { store } from "@/core/app/store";

class ApiProvider {
  constructor(config) {
    this.api = axios.create({
      ...config,
      baseURL: remoteServiceBaseApiUrl,
    });
    this.api.interceptors.request.use((req) => {
      return {
        ...req,
        headers: {
          ...req.headers,
          Accept: "*",
          Authorization: store.getState().credentials.token,
        },
      };
    });

    this.api.interceptors.response.use(
      (res) => res,
      (error) => {
        if (error.response.data.message) {
          console.log(error.response.data.message)
        }
      }
    );
  }

  async request(config) {
    return await this.api.request(config);
  }
}

export default ApiProvider;
