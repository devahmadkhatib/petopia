import axios from "axios";
import { store } from "@/core/app/store";

const client = axios.create({ baseURL: import.meta.env.VITE_BASE_URL });

export const request = async ({ ...options }) => {
  const state = store.getState();
  client.defaults.headers.common.Authorization = state.credentials.token;
  client.defaults.headers.common.Accept = "application/json";
  const onSuccess = (response) => response;
  const onError = (error) => {
    return Promise.reject(error);
  };

  return client(options).then(onSuccess).catch(onError);
};
