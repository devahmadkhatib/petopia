import { lazyLoad } from "@/core/helpers/lazyLoad";

export const publicRoutes = [
  { path: "/", element: lazyLoad("../pages/intro/index.jsx") },
];

export const privateRoutes = [
  // { path: "/", element: lazyLoad("@/core/pages/intro") },
];
