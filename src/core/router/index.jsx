import React from "react";
import { Route, Routes } from "react-router-dom";
import { ProtectedRoutes, PublicRoutes } from "@/core/routes";
import { privateRoutes, publicRoutes } from "./routesList";
import * as Comp from '../layout';
const withRoute = (routes) => {
  return routes.map((route) => (
    <Route path={route.path} element={<route.element />} key={route.path} />
  ));
};

function Router() {
  return (
    <>
    <Comp.Navbar />
    <Routes>
      <Route element={<PublicRoutes />}>{withRoute(publicRoutes)}</Route>
      <Route element={<ProtectedRoutes />}>{withRoute(privateRoutes)}</Route>
    </Routes>
    </>
  );
}

export default Router;
