import { APPProvider } from "./providers";
import Router from "./router";

function App() {
  return (
    <APPProvider>
      <Router />
    </APPProvider>
  );
}

export default App;
