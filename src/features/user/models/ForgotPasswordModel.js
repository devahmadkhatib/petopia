export const data = (data) => {
  return {
    id: data?.id,
    phone_number: data?.phoneNumber,
  };
};
