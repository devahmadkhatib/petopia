export const data = (data) => {
  return {
    token: data?.user_token,
    user: {
      id: data?.id,
      type: data?.user_type,
      name: data?.username,
      isActive: data?.is_active,
      image:
        data?.img_path === ""
          ? "/profile.jpg"
          : import.meta.env.VITE_BASE_URL + data?.img_path,
    },
  };
};
