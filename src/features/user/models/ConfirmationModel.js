export const data = (data) => {
  return {
    token: data?.Token,
    user: {
      id: data?.id,
      type: data?.userType,
      name: data?.name,
      isActive: data?.is_active,
      image:
        data?.img_path === ""
          ? "/profile.jpg"
          : import.meta.env.VITE_BASE_URL + data?.img_path,
    },
  };
};
