import { Box } from "@mui/system";
import { styled } from "@mui/material/styles";

export const ImageHolder = styled(Box)(({ theme, type }) => ({
  img: {
    ...(type !== "small" && {
      position: "absolute",
      top: "50%",
      right: 0,
      transform: "translate(-10%, -50%)",
      height: "90vh",
    }),
    boxShadow: "8px 8px 2px #ee91e2, -3px -0.5px 8px rgb(255, 251, 254)",
    borderRadius: 8,
    [theme.breakpoints.down("md")]: {
      height: "40vh",
      margin: "20px 0",
    },
    [theme.breakpoints.up("md")]: {
      ...(type === "small" && {
        display: "none",
      }),
    },
  },
}));
