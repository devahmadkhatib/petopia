import React from "react";
import { ImageHolder } from "./imageSection.styles";

function ImageSection({ src, type }) {
  return (
    <ImageHolder type={type}>
      <img src={src} alt="page-image"  />
    </ImageHolder>
  );
}

export default ImageSection;
