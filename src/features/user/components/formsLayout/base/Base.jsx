import React from "react";
import { StyledBase } from "./base.styles";

function Base({ children }) {
  return <StyledBase>{children}</StyledBase>;
}

export default Base;
