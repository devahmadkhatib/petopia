import { Stack } from "@mui/material";
import { styled } from "@mui/material/styles";

export const StyledBase = styled(Stack)(({ theme }) => ({
  width: "100%",
  gap: 20,

  "& .main-container": {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  "& .image-section": {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  [theme.breakpoints.up("md")]: {
    height: "100%",
    minHeight: "100%",
  },
  [theme.breakpoints.down("sm")]: {
    "& .main-container": {
      padding: "0 10px",
    },
  },
}));
