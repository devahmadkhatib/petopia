import { Divider } from "@mui/material";
import React from "react";
import { StyledFooter } from "./footer.styles";
import FacebookImg from "@/features/user/assets/images/facebook.png";
import GoogleImg from "@/features/user/assets/images/google.jpg";

function Footer() {
  return (
    <StyledFooter>
      <Divider>
        <img src={FacebookImg} alt="" />
        <img src={GoogleImg} alt="" />
      </Divider>
    </StyledFooter>
  );
}

export default Footer;
