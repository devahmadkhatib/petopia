import { styled } from "@mui/material/styles";

export const StyledFooter = styled("div")(({ theme }) => ({
  width: 432,
  img: {
    height: 15,
    margin: "0 8px",
  },
}));
