import { Stack } from "@mui/material";
import { styled } from "@mui/material/styles";

export const StyledContainer = styled(Stack)(({ theme }) => ({
  width: 432,
  border: `1px solid ${theme.palette.borders.dark}`,
  borderRadius: 10,
  padding: 24,
  marginBottom: 20,
  justifyContent: "center",
  height: "fit-content",
  alignItems: "center",
  form: {
    width: "100%",
  },
  [theme.breakpoints.down("sm")]: {
    width: "100%",
    padding: 10,
  },
}));
