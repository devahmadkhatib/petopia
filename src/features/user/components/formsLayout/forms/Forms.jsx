import React from "react";
import { StyledFormsArea } from "./forms.styles";

function FormsArea({ children }) {
  return <StyledFormsArea>{children}</StyledFormsArea>;
}

export default FormsArea;
