import { Stack } from "@mui/material";
import { styled } from "@mui/material/styles";

export const StyledFormsArea = styled("div")(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
  gap: 16,
  width: "100%",
}));
