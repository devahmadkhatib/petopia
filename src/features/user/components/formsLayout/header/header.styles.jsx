import { styled } from "@mui/material/styles";

export const StyledHeader = styled("div")(({ theme }) => ({
  position: "relative",
  width: "100%",
  display: "flex",
  justifyContent: "center",
  padding: "10px",
  "& .MuiButtonBase-root": {
    color: theme.palette.icons.pink,
    position: "absolute",
    top: "50%",
    left: "10%",
    fontSize: 30,
    transform: "translate(0, -50%)",
  },
  [theme.breakpoints.down("lg")]: {
    "& .MuiTypography-root": { fontSize: 20 },
    "& .MuiButtonBase-root": {
      left: "0%",
    },
  },
  [theme.breakpoints.down("sm")]: {
    "& .MuiTypography-root": {
      fontSize: 18,
    },
  },
}));
