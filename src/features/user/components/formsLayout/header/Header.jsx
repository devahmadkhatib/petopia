import React from "react";
import { IconButton, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import * as icons from "@/core/config/import/icons";
import { StyledHeader } from "./header.styles";

function Header(props) {
  const { title } = props;
  const navigate = useNavigate();

  return (
    <StyledHeader>
      <IconButton size="large" onClick={() => navigate("/intro")}>
        <icons.ArrowBackIcon fontSize="inherit" />
      </IconButton>
      <Typography variant="h2">{title}</Typography>
    </StyledHeader>
  );
}

export default Header;
