import React from "react";
import { StyledActions } from "./actions.styles";

function ActionsArea({ children }) {
  return <StyledActions>{children}</StyledActions>;
}

export default ActionsArea;
