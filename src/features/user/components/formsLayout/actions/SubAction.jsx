import React from "react";
import { StyledSubActions } from "./actions.styles";

function SubActionsArea({ children }) {
  return <StyledSubActions>{children}</StyledSubActions>;
}

export default SubActionsArea;
