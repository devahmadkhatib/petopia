import { Stack } from "@mui/material";
import { styled } from "@mui/material/styles";

export const StyledActions = styled(Stack)(({ theme }) => ({
  marginTop: 16,
}));

export const StyledSubActions = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  width: "100%",
  marginTop: 10,
}));
