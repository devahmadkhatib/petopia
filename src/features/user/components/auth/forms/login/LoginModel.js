import { useUserForm } from "@/features/user/hooks";
import { useLogin } from "@/features/user/services";

export const loginControl = () => {
  const { handleSubmit } = useUserForm();
  const goToRegister = () => handleSubmit(1, "/auth/register");
  const goToForgot = () => handleSubmit(2, "/auth/forgot_password");

  const { mutate: login, isLoading } = useLogin();

  const formSubmission = (values) => {
    login(
      JSON.stringify({
        body: {
          ...values,
        },
      })
    );
  };

  return { goToRegister, goToForgot, formSubmission, isLoading };
};
