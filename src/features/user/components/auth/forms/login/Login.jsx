import React from "react";
import { loginControl } from "./LoginModel";
import { Button, Field } from "@/core/components";
import { Typography } from "@mui/material";
import * as forms from "@/core/config/import/forms";
import * as validations from "@/core/config/import/validations";
import * as icons from "@/core/config/import/icons";
import CustomForm from "../customForm/CustomForm";

function Login() {
  const { goToRegister, goToForgot, formSubmission, isLoading } =
    loginControl();

  return (
    <CustomForm
      header="Login"
      mainAction="Login"
      isLoading={isLoading}
      form={forms.login().map((field, idx) => (
        <Field key={idx} {...field} />
      ))}
      subActions={
        <>
          <Typography variant="h6" color="gray" onClick={goToForgot}>
            forgot password?
          </Typography>
          <Button
            control="normal"
            endIcon={<icons.ArrowForwardIcon />}
            onClick={goToRegister}
          >
            Sign Up
          </Button>
        </>
      }
      formProps={{
        form: forms.login(),
        onSubmit: formSubmission,
        validationSchema: validations.login,
      }}
    />
  );
}

export default Login;
