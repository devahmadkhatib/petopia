import React from "react";
import { Button, Field } from "@/core/components";
import * as forms from "@/core/config/import/forms";
import * as validations from "@/core/config/import/validations";
import * as icons from "@/core/config/import/icons";
import CustomForm from "../customForm/CustomForm";
import { forgotPasswordControl } from "./ForgotPasswordModel";

function ForgotPassword() {
  const { goToLogin, formSubmission, isLoading } = forgotPasswordControl();

  return (
    <CustomForm
      header="Forgot Password?"
      mainAction="Next"
      isLoading={isLoading}
      form={forms.forgotPassword().map((field, idx) => (
        <Field key={idx} {...field} />
      ))}
      subActions={
        <Button
          control="normal"
          startIcon={<icons.ArrowBackIcon />}
          onClick={goToLogin}
        >
          BackToLogin
        </Button>
      }
      formProps={{
        form: forms.forgotPassword(),
        onSubmit: formSubmission,
        validationSchema: validations.forgotPassword,
      }}
    />
  );
}

export default ForgotPassword;
