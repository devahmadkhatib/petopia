import { useUserForm } from "@/features/user/hooks";
import { useForgotPassword } from "@/features/user/services";

export const forgotPasswordControl = () => {
  const { handleSubmit } = useUserForm();
  const goToLogin = () => handleSubmit(0, "/auth/login");

  const { mutate: forgotPassword, isLoading } = useForgotPassword();

  const formSubmission = (values) => {
    forgotPassword(
      JSON.stringify({
        body: {
          ...values,
        },
      })
    );
  };

  return { goToLogin, formSubmission, isLoading };
};
