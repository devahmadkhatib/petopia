import { useUserForm } from "@/features/user/hooks";
import { useConfirmation, useForgotPassword } from "@/features/user/services";
import { shallowEqual, useSelector } from "react-redux";
import * as selectors from "@/core/config/import/selectors";

export const confirmationControl = () => {
  const { handleSubmit } = useUserForm();
  const goToLogin = () => handleSubmit(0, "/auth/login");
  const goToResetForgot = () => handleSubmit(3, "/auth/reset_password");
  const forgotData = useSelector(selectors.userVerificationData, shallowEqual);

  const { mutate: confirm, isLoading } = useConfirmation();
  const { mutate: resend, isLoading: isResending } = useForgotPassword();

  const resendCode = () => {
    resend(
      JSON.stringify({
        body: {
          phone_number: forgotData.phone_number,
        },
      })
    );
  };

  const formSubmission = (values) => {
    values.id = forgotData.id;
    confirm(
      JSON.stringify({
        body: {
          ...values,
        },
      })
    );
  };

  return {
    goToLogin,
    goToResetForgot,
    formSubmission,
    resendCode,
    isResending,
    isLoading,
  };
};
