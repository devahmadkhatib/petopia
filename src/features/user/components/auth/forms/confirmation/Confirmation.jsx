import React from "react";
import { Button, Field } from "@/core/components";
import * as forms from "@/core/config/import/forms";
import * as validations from "@/core/config/import/validations";
import * as icons from "@/core/config/import/icons";
import CustomForm from "../customForm/CustomForm";
import { confirmationControl } from "./ConfirmationModel";
import { Typography } from "@mui/material";

function Confirmation() {
  const {
    goToLogin,
    goToResetForgot,
    formSubmission,
    isLoading,
    isResending,
    resendCode,
  } = confirmationControl();

  return (
    <CustomForm
      header="Forgot Password?"
      mainAction="Next"
      isLoading={isLoading}
      form={
        <>
          <Typography variant="h6">Enter The Code We've Sent You!</Typography>
          {forms.confirmation().map((field, idx) => (
            <Field key={idx} {...field} />
          ))}
          <div>
            <Button
              control="normal"
              startIcon={<icons.ResendIcon />}
              loadingPosition="start"
              loading={isResending}
              onClick={resendCode}
            >
              Resend Code
            </Button>
          </div>
        </>
      }
      subActions={
        <>
          <Button
            control="normal"
            startIcon={<icons.ArrowBackIcon />}
            onClick={goToLogin}
          >
            Back To Login
          </Button>
          <Typography variant="h6" color="gray" onClick={goToResetForgot}>
            change Number
          </Typography>
        </>
      }
      formProps={{
        form: forms.confirmation(),
        onSubmit: formSubmission,
        validationSchema: validations.confirmation,
      }}
    />
  );
}

export default Confirmation;
