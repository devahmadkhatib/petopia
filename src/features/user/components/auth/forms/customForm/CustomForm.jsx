import React from "react";
import { Button, Form } from "@/core/components";
import {
  ActionsArea,
  BaseForm,
  FormContainer,
  FormFooter,
  FormHeader,
  FormsArea,
  SubActionsArea,
} from "@/features/user/components/formsLayout";
import { FormPageImage } from "../../..";
import * as selectors from "@/core/config/import/selectors";
import InfoImage from "@/features/user/assets/images/info.png";
import CodeImage from "@/features/user/assets/images/code.png";
import { useSelector } from "react-redux";

function CustomForm({
  header,
  form,
  mainAction,
  formProps,
  subActions,
  isLoading = false,
}) {
  const image = useSelector(selectors.currentImage);
  const images = [InfoImage, CodeImage];

  return (
    <BaseForm>
      <FormHeader title={header} />
      <div className="image-section">
        <FormPageImage src={images[image]} type="small" />
      </div>
      <div className="main-container">
        <FormContainer>
          <Form enableReinitialize {...formProps}>
            <FormsArea>
              {form}
              <ActionsArea>
                <Button
                  loading={isLoading}
                  type="submit"
                  control="gradient"
                  gradient="blue"
                  variant="contained"
                  full
                >
                  {mainAction}
                </Button>
              </ActionsArea>
            </FormsArea>
          </Form>
          <SubActionsArea>{subActions}</SubActionsArea>
        </FormContainer>
        <FormFooter />
      </div>
    </BaseForm>
  );
}

export default CustomForm;
