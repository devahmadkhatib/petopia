import { useUserForm } from "@/features/user/hooks";
import { useRegister } from "@/features/user/services";

export const registerControl = () => {
  const { handleSubmit } = useUserForm();
  const goToLogin = () => handleSubmit(0, "/auth/login");

  const { mutate: register, isLoading } = useRegister();

  const formSubmission = (values) => {
    delete values.confirm_password
    register(
      JSON.stringify({
        body: {
          ...values,
        },
      })
    );
  };

  return { goToLogin, formSubmission, isLoading };
};
