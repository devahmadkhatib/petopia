import React from "react";
import { registerControl } from "./registerModel";
import { Button, Field } from "@/core/components";
import * as forms from "@/core/config/import/forms";
import * as validations from "@/core/config/import/validations";
import * as icons from "@/core/config/import/icons";
import CustomForm from "../customForm/CustomForm";

function Register() {
  const { goToLogin, formSubmission, isLoading } = registerControl();
  // console.log(typeof formSubmission);
  return (
    <CustomForm
      header="Register"
      mainAction="register"
      isLoading={isLoading}
      form={forms.register().map((field, idx) => (
        <Field key={idx} {...field} />
      ))}
      subActions={
        <Button
          control="normal"
          startIcon={<icons.ArrowBackIcon />}
          onClick={goToLogin}
        >
          Login
        </Button>
      }
      formProps={{
        form: forms.register(),
        onSubmit: formSubmission,
        validationSchema: validations.register,
      }}
    />
  );
}

export default Register;
