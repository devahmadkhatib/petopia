import { Stack, styled } from "@mui/material";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";

export const FormsSwiper = styled(Swiper)(({ theme }) => ({
  height: "100%",
  width: "100%",
}));

export const FormSlide = styled(SwiperSlide)(({ theme }) => ({
  height: "100%",
  width: "100%",
  overflowY: "scroll",
  "&::-webkit-scrollbar": {
    display: "none",
  },
}));

export const FormsHolder = styled(Stack)(({ theme }) => ({}));
