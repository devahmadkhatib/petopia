import React from "react";
import { useSelector } from "react-redux";
import { FormSlide, FormsSwiper } from "./formsPlayground.styles";
import Login from "../login/Login";
import Register from "../register/Register";
import * as selectors from "@/core/config/import/selectors";
import ForgotPassword from "../forgotPassword/ForgotPassword";
import ResetPassword from "../resetPassword/ResetPassword";
import { useParams } from "react-router-dom";
import { useUserForm } from "@/features/user/hooks";
import Confirmation from "../confirmation/Confirmation";

function FormsPlayground() {
  const swiperRef = React.useRef(null);
  const currentPage = useSelector(selectors.currentAuthPage);
  const { form } = useParams();
  const { handleCurrentForm } = useUserForm();
  const AuthForms = [
    Login,
    Register,
    ForgotPassword,
    ResetPassword,
    Confirmation,
  ];

  React.useEffect(() => {
    handleCurrentForm();
  }, [form]);

  React.useEffect(() => {
    swiperRef.current.swiper.slideTo(currentPage);
  }, [currentPage]);

  return (
    <FormsSwiper allowTouchMove={false} ref={swiperRef}>
      {AuthForms.map((Form, idx) => (
        <FormSlide key={idx}>
          <Form />
        </FormSlide>
      ))}
    </FormsSwiper>
  );
}

export default FormsPlayground;
