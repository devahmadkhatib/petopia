import React from "react";
import { Button, Field } from "@/core/components";
import * as forms from "@/core/config/import/forms";
import * as validations from "@/core/config/import/validations";
import * as icons from "@/core/config/import/icons";
import CustomForm from "../customForm/CustomForm";
import { resetPasswordControl } from "./ResetPasswordModel";

function ResetPassword() {
  const { goToLogin, formSubmission } = resetPasswordControl();

  return (
    <CustomForm
      header="Reset Password"
      mainAction="Next"
      form={forms.resetPassword().map((field, idx) => (
        <Field key={idx} {...field} />
      ))}
      subActions={
        <Button
          control="normal"
          startIcon={<icons.ArrowBackIcon />}
          onClick={goToLogin}
        >
          Back To Login
        </Button>
      }
      formProps={{
        form: forms.resetPassword(),
        onSubmit: formSubmission,
        validationSchema: validations.resetPassword,
      }}
    />
  );
}

export default ResetPassword;
