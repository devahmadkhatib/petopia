import { useUserForm } from "@/features/user/hooks";

export const resetPasswordControl = () => {
  const { handleSubmit } = useUserForm();
  const goToLogin = () => handleSubmit(0, "/auth/login");

  const formSubmission = (values) => {};

  return { goToLogin, formSubmission };
};
