export const form = () => {
  return [
    {
      control: "input",
      type: "password",
      placeholder: "new password",
      name: "password",
    },
    {
      control: "input",
      type: "password",
      placeholder: "confirm password",
      name: "confirm_password",
    },
  ];
};
