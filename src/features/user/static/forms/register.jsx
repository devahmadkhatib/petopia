export const form = () => {
  return [
    {
      control: "select",
      type: "text",
      name: "user_type",
      placeholder: "who are you?",
      options: [
        { value: 0, label: "Student" },
        { value: 1, label: "Teacher" },
        { value: 2, label: "Organization" },
      ],
    },
    {
      control: "input",
      type: "text",
      placeholder: "Username",
      name: "username",
    },
    {
      control: "input",
      type: "text",
      placeholder: "Email",
      name: "email",
    },
    {
      control: "phoneNumber",
      type: "text",
      placeholder: "000-000-000",
      name: "mobile_number",
    },
    {
      control: "input",
      type: "password",
      placeholder: "Password",
      name: "password",
    },
    {
      control: "input",
      type: "password",
      placeholder: "Confirm Password",
      name: "confirm_password",
    },
  ];
};
