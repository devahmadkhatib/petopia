export const form = () => {
  return [
    {
      control: "phoneNumber",
      type: "text",
      placeholder: "000-000-000",
      name: "phone_number",
    },
  ];
};
