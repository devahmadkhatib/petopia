export { form as login } from "./login";
export { form as register } from "./register";
export { form as resetPassword } from "./resetPassword";
export { form as forgotPassword } from "./forgotPassword";
export { form as confirmation } from "./confirmation";
