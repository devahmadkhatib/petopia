import * as Yup from "yup";
import * as regex from "@/core/config/static/regex";

const validationSchema = Yup.object({
  password: Yup.string()
    .required("Please enter your password")
    .min(8, "Password must contain at least 8 characters")
    .matches(
      /(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}/,
      "Password must contain at least one uppercase letter, one symbol, and one number"
    ),
  confirm_password: Yup.string()
    .oneOf([Yup.ref("password"), null], "Passwords must match")
    .required("Please confirm your password"),
});

export default validationSchema;
