import * as YUP from "yup";

const validationSchema = YUP.object({
  code: YUP.string().min(6, "not valid").required("please enter your code"),
});

export default validationSchema;
