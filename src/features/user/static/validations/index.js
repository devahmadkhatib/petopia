export { default as login } from "./login";
export { default as register } from "./register";
export { default as forgotPassword } from "./forgotPassword";
export { default as resetPassword } from "./resetPassword";
export { default as confirmation } from "./confirmation";
