import * as YUP from "yup";
import * as regex from "@/core/config/static/regex";

const validationSchema = YUP.object({
  phone_number: YUP.string()
    .matches(regex.phoneRegExp, "not valid")
    .min(12, "not valid")
    .required("please enter your phone number"),
});

export default validationSchema;
