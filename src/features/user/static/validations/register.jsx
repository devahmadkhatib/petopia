import * as Yup from "yup";
import * as regex from "@/core/config/static/regex";

const validationSchema = Yup.object({
  user_type: Yup.string().required("Please select your role"),
  username: Yup.string().required("Please enter your username"),
  email: Yup.string()
    .email("Please enter a valid email address")
    .required("Please enter your email"),
  mobile_number: Yup.string()
    .matches(regex.phoneRegExp, "Please enter a valid phone number")
    // .min(9, "Please enter a valid phone number")
    // .max(9, "Please enter a valid phone number")
    .required("Please enter your phone number"),
  password: Yup.string()
    .required("Please enter your password")
    .min(8, "Password must contain at least 8 characters")
    .matches(
      /(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}/,
      "Password must contain at least one uppercase letter, one symbol, and one number"
    ),
  confirm_password: Yup.string()
    .oneOf([Yup.ref("password"), null], "Passwords must match")
    .required("Please confirm your password"),
});

export default validationSchema;
