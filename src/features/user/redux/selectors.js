export const userToken = (state) => state.credentials.token;
export const userData = (state) => state.credentials.user;
export const currentAuthPage = (state) => state.auth.currentPage;
export const currentImage = (state) => state.auth.currentImage;
export const userVerificationData = (state) => state.credentials.forgotData;
