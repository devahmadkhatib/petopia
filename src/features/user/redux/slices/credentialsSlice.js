import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  forgotData: {},
  user: {},
  token: null,
};

const credentialsSlice = createSlice({
  name: "credentials",
  initialState: initialState,
  reducers: {
    setCredentials: (state, action) => {
      const { token, user } = action.payload;
      state.user = user;
      state.token = token;
    },
    setForgotData: (state, action) => {
      state.forgotData = action.payload;
    },
    logout: (state, action) => {
      state.user = {};
      state.token = null;
    },
  },
});

export const { setCredentials, setForgotData, logout } =
  credentialsSlice.actions;
export default credentialsSlice.reducer;
