import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  currentPage: 0,
  currentImage: 0,
};

const authSlice = createSlice({
  name: "auth",
  initialState: initialState,
  reducers: {
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    },
    setCurrentPageImage: (state, action) => {
      state.currentImage = action.payload;
    },
  },
});

export const { setCurrentPage } = authSlice.actions;
export default authSlice.reducer;
