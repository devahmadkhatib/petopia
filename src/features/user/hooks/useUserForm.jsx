import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import * as actions from "@/core/config/import/actions";

export const useUserForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { form } = useParams();

  const handleSubmit = (form, location) => {
    dispatch(actions.setCurrentPage(form));
    navigate(location);
  };

  const handleCurrentForm = () => {
    switch (form) {
      case "login":
        dispatch(actions.setCurrentPage(0));
        return;
      case "register":
        dispatch(actions.setCurrentPage(1));
        return;
      case "forgot_password":
        dispatch(actions.setCurrentPage(2));
        return;
      case "reset_password":
        dispatch(actions.setCurrentPage(3));
        return;
      case "confirmation":
        dispatch(actions.setCurrentPage(4));
        return;
      default:
        dispatch(actions.setCurrentPage(0));
        return;
    }
  };

  return { handleSubmit, handleCurrentForm };
};
