import { request } from "@/core/utils/axios";
import { useMutation } from "@tanstack/react-query";
import * as constants from "@/core/config/static/constants";
import * as actions from "@/core/config/import/actions";
import { useDispatch } from "react-redux";
import { ForgotPasswordModel, UserModel } from "../../models";
import { useState } from "react";
import { useUserForm } from "../../hooks";

const forgotPassword = (payload) => {
  return request({
    url: `/resend_code`,
    method: "POST",
    data: payload,
    headers: constants.jsonHeader,
  });
};

export const useForgotPassword = () => {
  const dispatch = useDispatch();
  const [phoneNumber, setPhoneNumber] = useState("");
  const { handleSubmit } = useUserForm();
  return useMutation(forgotPassword, {
    onMutate: (data) => {
      const params = JSON.parse(data);
      setPhoneNumber(params.body.phone_number);
    },
    onSuccess: (data) => {
      dispatch(
        actions.setForgotData(
          ForgotPasswordModel({ id: data.data.ID, phoneNumber })
        )
      );
      handleSubmit(4, "/auth/confirmation");
    },
  });
};
