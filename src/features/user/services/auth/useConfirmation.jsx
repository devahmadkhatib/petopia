import { request } from "@/core/utils/axios";
import { useMutation } from "@tanstack/react-query";
import * as constants from "@/core/config/static/constants";
import * as actions from "@/core/config/import/actions";
import { useDispatch } from "react-redux";
import { ConfirmationModel } from "../../models";

const confirm = (payload) => {
  return request({
    url: `/verification`,
    method: "POST",
    data: payload,
    headers: constants.jsonHeader,
  });
};

export const useConfirmation = () => {
  const dispatch = useDispatch();

  return useMutation(confirm, {
    onSuccess: (data) => {
      dispatch(actions.setCredentials(ConfirmationModel(data.data)));
    },
  });
};
