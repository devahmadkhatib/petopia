import { request } from "@/core/utils/axios";
import { useMutation } from "@tanstack/react-query";
import * as constants from "@/core/config/static/constants";
import * as actions from "@/core/config/import/actions";
import { useDispatch } from "react-redux";
import { RegisterModel } from "../../models";

const register = (payload) => {
  return request({
    url: `/register`,
    method: "POST",
    data: payload,
    headers: constants.jsonHeader,
  });
};

export const useRegister = () => {
  const dispatch = useDispatch();

  return useMutation(register, {
    onSuccess: (data) => {
      dispatch(actions.setCredentials(RegisterModel(data.data.details)));
    },
  });
};
