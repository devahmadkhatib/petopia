import { request } from "@/core/utils/axios";
import { useMutation } from "@tanstack/react-query";
import * as constants from "@/core/config/static/constants";
import * as actions from "@/core/config/import/actions";
import { useDispatch } from "react-redux";
import { UserModel } from "../../models";

const login = (payload) => {
  return request({
    url: `/log_in`,
    method: "POST",
    data: payload,
    headers: constants.jsonHeader,
  });
};

export const useLogin = () => {
  const dispatch = useDispatch();

  return useMutation(login, {
    onSuccess: (data) => {
      dispatch(actions.setCredentials(UserModel(data.data)));
    },
  });
};
