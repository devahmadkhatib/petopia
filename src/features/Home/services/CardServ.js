import ApiService from "@/core/utils/base-api/api-service"

class CardSerService extends ApiService {
    getCardServices() {
        return this.get("services")
    }
   
}

export const cardSerService = new CardSerService