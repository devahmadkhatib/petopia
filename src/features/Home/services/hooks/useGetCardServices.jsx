import { useQuery } from "@tanstack/react-query"
import { cardSerService } from "../CardServ"

export const useGetCardService = () => {
    return useQuery({
        queryFn: () => cardSerService.getCardServices(),
        queryKey: [`getCardService`]
    })
}