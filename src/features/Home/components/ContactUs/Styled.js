import { Box, Typography, styled } from "@mui/material";

export const ContactUsContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'space-evenly',
    gap: '32px',
    height: '450px',
    backgroundColor: '#FBF9FF',
    [theme.breakpoints.down('md')]: {
        height: 'auto',
        padding:'16px 0'
    }
}));
export const ContactUsCards = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    width: '80%',
    justifyContent: 'space-between',
    gap: '32px',
    flexWrap: 'wrap',
    [theme.breakpoints.down('md')]: {
        width: '95%'
    },
    [theme.breakpoints.down('ss')]: {
        justifyContent:'center'
    }
}));
export const ContactUsCard = styled(Box)(({ theme }) => (props) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    gap: '4px',
    width: '22%',
    borderRadius: '8px',
    position: 'relative',
    height: '170px',
    border: `2px solid ${props.background ? props.background : '#EBE5F7'}`,
    background: theme.palette.primary.light,
    marginTop: "38px",
    flexDirection: 'column',
    [theme.breakpoints.down('md')]: {
        width: '40%'
    },
    [theme.breakpoints.down('sm')]: {
        width: '45%'
    },
    [theme.breakpoints.down('ss')]: {
        width: '95%'
    },
}));
export const ContactUsImg = styled(Box)(({ theme }) => (props) => ({
    width: '80px',
    height: '80px',
    borderRadius: '50%',
    backgroundColor: props.background ? props.background : '#EBE5F7',
    border: '2px solid #EBE5F7',
    position: 'absolute ',
    top: '-50px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
}));
export const ContactUsTitle = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.dark,
    textAlign: "center",
    fontSize: "22px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "30px"
}));
export const ContactUsInfo = styled(Typography)(({ theme }) => ({
    color: "#1C103B",
    textAlign: "center",
    fontSize: "18px",
    fontFamily: "Lato",
    fontStyle: "normal",
    fontWeight: 400,
    lineHeight: "25px"
}));