import React from 'react'
import * as Styles from './Styled';
import * as Icons from '@/core/config/import/icons';
import { Icon } from '@mui/material';

const Card = ({ title, disc1, disc2, id, color, background }) => {
    return (
        <Styles.ContactUsCard background={background}>
            <Styles.ContactUsImg background={background}>
                {
                    id == 1 ? <Icons.CallIcon style={{ color: color, width: '30px' }} />
                        : id == 2 ? <Icons.EmailIcon style={{ color: color, width: '30px' }} />
                            : id == 3 ? <Icons.PlaceIcon style={{ color: color, width: '30px' }} />
                                : <Icons.WatchLaterIcon style={{ color: color, width: '30px' }} />
                }
            </Styles.ContactUsImg>
            <Styles.ContactUsTitle>
                {title}
            </Styles.ContactUsTitle>
            <Styles.ContactUsInfo>
                {disc1}
            </Styles.ContactUsInfo>
            <Styles.ContactUsInfo>
                {disc2}
            </Styles.ContactUsInfo>
        </Styles.ContactUsCard>)
}

export default Card