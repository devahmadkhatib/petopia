import React from 'react'
import * as Styles from './Styled';
import Header from '../Shared/header';
import Card from './Card';
import { dataContact } from '@/features/Home/assets/data/dataContact';
const ContactUS = () => {
    return (
        <Styles.ContactUsContainer>
            <Header Title={'Contacts'} disc={'Massa enim nec dui nunc mattis enim ut tellus'} hint={'OUR CONTACTS'} />
            <Styles.ContactUsCards>
                {
                    dataContact.map((card) =>
                        <Card key={card.id}
                            title={card.title}
                            color={card.color}
                            disc1={card.disc1}
                            disc2={card.disc2}
                            id={card.id}
                            background={card.backColor}
                        />
                    )
                }
            </Styles.ContactUsCards>
        </Styles.ContactUsContainer>
    )
}

export default ContactUS