import * as Images from '@/features/Home/assets/images';
import { Box, Stack, Typography, styled } from '@mui/material';
export const Banaras = styled(Stack)(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
}));
export const BanarasContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundImage: `url("${Images.bgBanaras}")`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    height: '180px',
    width: '80%',
    marginTop: '16px',
    borderRadius: '8px',
    flexWrap: 'wrap',
    [theme.breakpoints.down('md')]: {
       height:'auto',
       width:'95%'
    }
}));
export const BanarasCard = styled(Box)(({ theme }) => ({
    display: 'flex',
    gap: '16px',
    alignItems: 'center',
    width: '20%',
    [theme.breakpoints.down('md')]: {
        width: '40%',
        margin: '16px 0'
    },
    [theme.breakpoints.down('sm')]: {
        width: '80%',
        margin: '16px 0'
    }
}));
export const BanarasCardText = styled(Box)(({ theme }) => ({
    display: 'flex',
    gap: '4px',
    flexDirection: 'column'
}));
export const BanarasTextTitle = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.light,
    fontSize: "46px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "46px",
    [theme.breakpoints.down('md')]: {
        fontSize: "32px",
        lineHeight: "34px",
    }
}));
export const BanarasTextDisc = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.light,
    fontSize: "18px",
    fontStyle: "normal",
    fontWeight: 400,
    lineHeight: "26px"
}));
export const BanarasImg = styled('img')(({ theme }) => ({
    width: '56px',
    [theme.breakpoints.down('md')]: {
        width: '48px'
    }
}));