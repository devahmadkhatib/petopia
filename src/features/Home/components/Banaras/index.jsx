import React from 'react'
import * as Styles from './StyledBanas';
import Card from './Card';
import { dataBan } from '@/features/Home/assets/data/dataBanars';
const Banaras = () => {
  return (
    <Styles.Banaras>
      <Styles.BanarasContainer>
        {
          dataBan.map((card) =>
            <Card key={card.id} title={card.title} disc={card.disc} img={card.img} />
          )
        }
      </Styles.BanarasContainer>
    </Styles.Banaras>
  )
}

export default Banaras