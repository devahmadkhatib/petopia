import React from 'react'
import * as Styles from './StyledBanas';
const Card = ({ img, title, disc }) => {
    return (
        <Styles.BanarasCard>
            <Styles.BanarasImg src={img} />
            <Styles.BanarasCardText>
                <Styles.BanarasTextTitle>
                    {title}+
                </Styles.BanarasTextTitle>
                <Styles.BanarasTextDisc>
                    {disc}
                </Styles.BanarasTextDisc>
            </Styles.BanarasCardText>
        </Styles.BanarasCard>
    )
}

export default Card