import { Box, Typography, styled } from "@mui/material";

export const HeaderContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
}));
export const HeaderHint = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.main,
    fontSize: "16px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "19px",
    [theme.breakpoints.down('md')]: {
        fontSize: '14px',
        lineHeight: "16px",
    }
}));
export const HeaderText = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.dark,
    textAlign: "center",
    fontSize: "46px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "46px",
    [theme.breakpoints.down('md')]: {
        fontSize: '32px',
        lineHeight: "36px",
    }
}));

export const HeaderHintDisc = styled(Typography)(({ theme }) => ({
    color: "#1C103B",
    textAlign: "center",
    fontSize: "18px",
    fontStyle: "normal",
    fontWeight: 400,
    lineHeight: "25px"
}));