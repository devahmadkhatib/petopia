import React from 'react'
import * as Styles from './StyledHeader';
const Header = ({ hint, Title, disc }) => {
    return (
        <Styles.HeaderContainer>
            {
                hint &&
                <Styles.HeaderHint>
                    {hint}
                </Styles.HeaderHint>
            }

            <Styles.HeaderText>
                {Title}
            </Styles.HeaderText>
            {
                disc &&
                <Styles.HeaderHintDisc>
                    {disc}
                </Styles.HeaderHintDisc>
            }

        </Styles.HeaderContainer>
    )
}

export default Header