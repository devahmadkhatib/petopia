import React from 'react'
import * as Icons from '@/core/config/import/icons';
import * as Styles from './Style';

const Card = ({ img, hint, title, user, data, disc }) => {
    return (
        <Styles.BlogCard>
            <Styles.BlogCardImg>
                <Styles.BlogImg src={img} />
                <Styles.BlogHint >
                    {hint}
                </Styles.BlogHint>
            </Styles.BlogCardImg>
            <Styles.BlogInfo>
                <Styles.BlogInfoText >
                    <Icons.PersonIcon style={{ width: '15px', color: '#7C58D3' }} />
                    <Styles.BlogTextInfo>
                        {user}
                    </Styles.BlogTextInfo>
                </Styles.BlogInfoText>
                <Styles.BlogInfoText >
                    <Icons.TodayIcon style={{ width: '15px', color: '#7C58D3' }} />
                    <Styles.BlogTextInfo>
                        {data}
                    </Styles.BlogTextInfo>
                </Styles.BlogInfoText>
            </Styles.BlogInfo>
            <Styles.BlogInfoTextBox >
                <Styles.TextInfoHeading>
                    {title}
                </Styles.TextInfoHeading>
                <Styles.TextInfoDisc>
                    {disc}
                </Styles.TextInfoDisc>
            </Styles.BlogInfoTextBox>
        </Styles.BlogCard>
    )
}

export default Card