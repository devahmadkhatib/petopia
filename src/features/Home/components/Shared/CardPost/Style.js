import { Box, Typography, styled } from "@mui/material";
export const BlogCard = styled(Box)(({ theme }) => ({
    display: 'flex',
    width: '32%',
    flexDirection: 'column',
    gap: '16px',
    height: '450px',
    [theme.breakpoints.down('md')]: {
        width: '47%',
        height: 'auto',
    },
    [theme.breakpoints.down('sm')]: {
        width: '95%'
    }
}));
export const BlogCardImg = styled(Box)(({ theme }) => ({
    height: '200px',
    width: '100%',
    position: 'relative',
}));
export const BlogImg = styled('img')(({ theme }) => ({
    height: '100%',
    width: '100%'

}));
export const BlogHint = styled(Box)(({ theme }) => ({
    position: 'absolute',
    top: '-15px',
    right: '20px',
    padding: '8px',
    borderRadius: '8px',
    background: '#FFDA47',
    color: "#1C103B",
    textAlign: "right",
    fontSize: "16px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "19px",
    [theme.breakpoints.down('sm')]: {
        fontSize: "14px",
        lineHeight: "16px",
        padding: '5px',
        top: '-10px',
    }
}));
export const BlogInfo = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: ' 0 8px',
    [theme.breakpoints.down('md')]: {
        flexDirection: 'column-reverse'
    },
    [theme.breakpoints.down('sm')]: {
        flexDirection: 'row'
    }
}));
export const BlogInfoText = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    gap: '4px',

}));
export const BlogInfoTextBox = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    gap: '0px',
    flexDirection: 'column',
    [theme.breakpoints.down('sm')]: {
        padding: ' 0 8px'
    }
}));
export const BlogTextInfo = styled(Typography)(({ theme }) => ({
    color: "#1C103B",
    fontSize: "16px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "19px",

    [theme.breakpoints.down('sm')]: {
        fontSize: "12px",
        lineHeight: "14px",

    }
}));
export const TextInfoHeading = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.dark,
    fontSize: "24px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "normal",
    [theme.breakpoints.down('sm')]: {
        fontSize: "20px",
    }
}));
export const TextInfoDisc = styled(Typography)(({ theme }) => ({
    color: "#1C103B",
    fontSize: "18px",
    fontStyle: "normal",
    fontWeight: 400,
    lineHeight: "26px",
    width: '98%',
    [theme.breakpoints.down('sm')]: {
        fontSize: "16px",
        lineHeight: "19px",
    }
}));