import React from 'react'
import * as Styles from './StyledServic';
import * as Comp from './component';
import Heder from '../Shared/header';
const Services = () => {
    return (
        <Styles.ServiceContainer>
            <Heder hint={'All Pet Care Services'} Title={'OUR SERVICES'} />
            <Comp.CardServices />
            <Comp.BanServices />
            <Comp.RateDog />
        </Styles.ServiceContainer>
    )
}

export default Services