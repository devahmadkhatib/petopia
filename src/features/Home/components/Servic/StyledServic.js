import { Box, styled } from "@mui/material";

export const ServiceContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'center',
    gap: '32px'
}));