import React from 'react'
import * as Styles from './StyledRateDog';
import { dataRate } from '@/features/Home/assets/data/dataRateCard';
import Card from './Card';
const RateDog = () => {
    return (
        <Styles.RateDogContainer>
            <Styles.RateDogContain>

                {
                    dataRate.map((card) =>
                        <Card key={card.id} Title={card.title} cards={card.cards} />
                    )
                }
            </Styles.RateDogContain>
        </Styles.RateDogContainer>
    )
}

export default RateDog