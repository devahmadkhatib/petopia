import React from 'react'
import * as Styles from './StyledRateDog';
import * as Comp from '@/core/components';

const Card = ({ Title, cards }) => {
    return (
        <Styles.RateCardDog>
            <Styles.RateCardDogTitle>
                {Title}
            </Styles.RateCardDogTitle>
            <Styles.CardsDog>
                {cards.map((card) =>
                    <Styles.CardDog key={card.id}>
                        <Styles.CardDogImg>
                            <Styles.CardImg src={card.img} />
                        </Styles.CardDogImg>
                        <Styles.CardText>
                            <Styles.CardTitle >
                                {card.title}
                            </Styles.CardTitle>
                            <Comp.RatingComponent value={5} readOnly={true} />
                            <Styles.CardPrice>
                                {card.price}
                            </Styles.CardPrice>
                        </Styles.CardText>
                    </Styles.CardDog>)
                }

            </Styles.CardsDog>
        </Styles.RateCardDog>
    )
}

export default Card