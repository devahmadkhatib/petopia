import { Box, Typography, styled } from "@mui/material";

export const RateDogContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: '80%',
    justifyContent: 'center',
    gap: '24px',
    [theme.breakpoints.down('lg')]: {
        width: '95%'

    }
}));
export const RateDogContain = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    flexWrap: 'wrap'
}));
export const RateCardDog = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '32px',
    width: '380px',
    [theme.breakpoints.down('ss')]: {
        width: '95%'
    }

}));
export const RateCardDogTitle = styled(Box)(({ theme }) => ({
    color: theme.palette.primary.dark,
    fontSize: "32px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "36px",
    margin: '24px 0 0',
    [theme.breakpoints.down('ss')]: {
        fontSize: "28px",
        lineHeight: "30px",
        margin: '32px 0 0'
    }
}));
export const CardsDog = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '8px',
    width: '100%',

}));
export const CardDog = styled(Box)(({ theme }) => ({
    width: '100%',
    height: '180px',
    display: 'flex',
    alignItems: 'center',
    gap: '16px',
    [theme.breakpoints.down('ss')]: {
        flexDirection: 'column',
        height: 'auto',
    }
}));
export const CardDogImg = styled(Box)(({ theme }) => ({
    width: '240px',
    borderRadius: '12px',
    maxWidth:'200px',
    minWidth:'180px',
    [theme.breakpoints.down('ss')]: {
        width: '220px'
    }
}));
export const CardImg = styled('img')(({ theme }) => ({
    width: '100%',
}));
export const CardText = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '8px',
    [theme.breakpoints.down('ss')]: {
        textAlign: 'center'
    }
}));
export const CardTitle = styled(Typography)(({ theme }) => ({
    color: "#1C103B",
    fontSize: "24px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "30px",
    [theme.breakpoints.down('ss')]: {
        fontSize: "18px",
        lineHeight: "20px",

    }
}));
export const CardPrice = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.main,
    fontSize: "22px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "30px",
    [theme.breakpoints.down('ss')]: {
        fontSize: "18px",
        lineHeight: "20px",
    }
}));