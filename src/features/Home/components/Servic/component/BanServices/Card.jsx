import React from 'react'
import * as Styles from './StyledBanServices';
import * as Icons from '@/core/config/import/icons';
import { ButtonBase } from '@mui/material';
const Card = ({ title, img }) => {
    return (
        <Styles.CardDog>
            <Styles.CardDogText>
                {title}
            </Styles.CardDogText>
            <Styles.RoseCardLink component={ButtonBase} >
                <Styles.RoseCardLinkFont>
                    Shop Now
                </Styles.RoseCardLinkFont>
                <Icons.ArrowForwardIcon style={{ color: '#7C58D3', width: '25px' }} />
            </Styles.RoseCardLink>
            <Styles.CardDogImg>
                <Styles.DogImg src={img} />
            </Styles.CardDogImg>
        </Styles.CardDog>
    )
}

export default Card