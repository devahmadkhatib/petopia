import { Box, Typography, styled } from "@mui/material";

export const BanServiceContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    width: '80%',
    justifyContent: 'space-between',
    gap: '20px',
    margin: '32px 0',
    [theme.breakpoints.down('lg')]: {
        flexDirection: 'column',
        width: '95%',
    },

}));
export const BanServiceCardRose = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '68%',
    height: '450px',
    background: '#FCDCB5',
    [theme.breakpoints.down('lg')]: {
        width: '100%'
    },
    [theme.breakpoints.down('md')]: {
        height: 'auto',
    }
}));
export const ContainersRoseCard = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    width: '85%',
    height: '100%',
    [theme.breakpoints.down('md')]: {
        flexDirection: 'column'

    }
}));
export const RoseCardText = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '12px',
    width: '38%',
    marginTop: '32px',
    [theme.breakpoints.down('md')]: {
        width: '100%',
        marginTop: '16px',
    }
}));
export const RoseCardLink = styled(Box)(({ theme }) => ({
    display: 'flex',
    gap: '12px',
    alignItems: 'center',
    padding: '0',
    width: 'fit-Content',
}));
export const RoseCardLinkFont = styled(Typography)(({ theme }) => ({
    color: "#7C58D3",
    fontSize: "20px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "30px"
}));
export const RoseCardTitle = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.dark,
    fontSize: "46px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "46px",
    [theme.breakpoints.down('sm')]: {
        fontSize: "32px",
        lineHeight: "32px",

    }
}));
export const RoseCardDisc = styled(Typography)(({ theme }) => ({
    color: "#1C103B",
    fontSize: "18px",
    fontStyle: "normal",
    fontWeight: 400,
    lineHeight: "26px"
}));
export const RoseCardImg = styled(Box)(({ theme }) => ({
    width: '65%'
}));
export const RoseImg = styled('img')(({ theme }) => ({
    width: '100%'
}));
export const BoxCard = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '12px',
    flex: '1',
    [theme.breakpoints.down('lg')]: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        gap: '12px',
        width: '95%'
    },
    [theme.breakpoints.down('md')]: {
        flexDirection: 'column',
    }
}));
export const CardDogText = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.dark,
    fontSize: "30px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "36px",
    width: '58%',
    [theme.breakpoints.down('ss')]: {
        fontSize: "23px",
    }
}));
export const CardDog = styled(Box)(({ theme }) => ({
    height: '210px',
    display: 'flex',
    backgroundColor: '#ffda47',
    width: '100%',
    borderRadius: '8px',
    padding: ' 0 0 0 8px',
    position: 'relative',
    // alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    gap: '16px',
    [theme.breakpoints.down('ss')]: {
        height: '170px',
    }
}));
export const CardDogImg = styled(Box)(({ theme }) => ({
    height: '218px',
    width: '170px',
    borderRadius: '8px',
    position: 'absolute',
    bottom: '-0px',
    right: '-10px',
    [theme.breakpoints.down('ss')]: {
        width: "130px",
        height: '180px',
    }
}));
export const DogImg = styled('img')(({ theme }) => ({
    height: '100%',
    width: '100%',
}));