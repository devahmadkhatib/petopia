import React from 'react'
import * as Styles from './StyledBanServices';
import * as Icons from '@/core/config/import/icons';
import * as Images from '@/features/Home/assets/images';
import { ButtonBase } from '@mui/material';
import { dataDog } from '@/features/Home/assets/data/dataDog';
import Card from './Card';
const BanServices = () => {
    return (
        <Styles.BanServiceContainer>
            <Styles.BanServiceCardRose>
                <Styles.ContainersRoseCard>
                    <Styles.RoseCardText>
                        <Styles.RoseCardTitle>
                            Сheck Out Our Specials
                        </Styles.RoseCardTitle>
                        <Styles.RoseCardDisc>
                            Massa placerat duis ultricies lacus. Aliquet bibendum enim facilisis gravida neque convallis
                        </Styles.RoseCardDisc>
                        <Styles.RoseCardLink component={ButtonBase} >
                            <Styles.RoseCardLinkFont>
                                Shop Now
                            </Styles.RoseCardLinkFont>
                            <Icons.ArrowForwardIcon style={{ color: '#7C58D3', width: '25px' }} />
                        </Styles.RoseCardLink>
                    </Styles.RoseCardText>
                    <Styles.RoseCardImg>
                        <Styles.RoseImg src={Images.DogBan} />
                    </Styles.RoseCardImg>
                </Styles.ContainersRoseCard>
            </Styles.BanServiceCardRose>
            <Styles.BoxCard>
                {
                    dataDog.map((card) =>
                        <Card key={card.id} title={card.title} img={card.img} />
                    )
                }
            </Styles.BoxCard>
        </Styles.BanServiceContainer>
    )
}

export default BanServices