import React from 'react'
import { ButtonBase } from '@mui/material';
import * as Styles from './StyledServices';
import * as Images from '@/features/Home/assets/images';
import * as Icons from '@/core/config/import/icons';
const Card = ({ img, background, title, disc, detail }) => {
    return (
        <Styles.ServiceCard background={background}>
            <Styles.ServiceCardBG>
                <img src={Images.paws} alt="" />
            </Styles.ServiceCardBG>
            <Styles.CardTitle >
                <Styles.CardTitleImg >
                    <Styles.CardImg src={img} />
                </Styles.CardTitleImg>
                <Styles.CardTitleText background={background}>
                    {title}
                </Styles.CardTitleText>
            </Styles.CardTitle>
            <Styles.CardDisc background={background}>
                {disc}
            </Styles.CardDisc>
            <Styles.CardDetails background={background}>
                {detail}
            </Styles.CardDetails>
            <Styles.RoseCardLink component={ButtonBase} >
                <Styles.RoseCardLinkFont background={background}>
                    Get Service
                </Styles.RoseCardLinkFont>
                <Icons.ArrowForwardIcon style={{ color: background !== '#fff' ? '#FFDA47' : '#7C58D3', width: '25px' }} />
            </Styles.RoseCardLink>
        </Styles.ServiceCard>
    )
}

export default Card