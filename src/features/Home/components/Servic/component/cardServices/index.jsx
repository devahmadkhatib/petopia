import React from 'react'
import * as Styles from './StyledServices';
import Card from './Card';
import { useGetCardService } from '@/features/Home/services/hooks/useGetCardServices';
import { Skeleton } from '@mui/material';
const CardServices = () => {
    const { data, isLoading } = useGetCardService();


    return (
        <Styles.ServiceContainer>
            <Styles.ServiceCards>
                {
                    !isLoading && data ?
                        data.data.map((card) =>
                            <Card key={card.id} img={card.img} title={card.title}
                                disc={card.disc}
                                detail={'From $39 / complex'}
                                background={card.background}
                            />
                        )
                        : <Styles.ServiceLoader>loading ...</Styles.ServiceLoader>
                }
            </Styles.ServiceCards>
        </Styles.ServiceContainer>
    )
}

export default CardServices