import { Box, Typography, styled } from "@mui/material";
import * as Images from '@/features/Home/assets/images';
export const ServiceContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'center',
    gap: '32px',
    marginTop: '32px'
}));
export const ServiceCards = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    width: '80%',
    justifyContent: 'space-between',
    gap: '24px',
    flexWrap: 'wrap',
    [theme.breakpoints.down('lg')]: {
        width: '95%'
    },
    [theme.breakpoints.down('ss')]: {
        justifyContent: 'center'
    },
}));
export const ServiceLoader = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '300px',
    width: '80%'
}));
export const ServiceCard = styled(Box)(({ theme }) => (props) => ({
    display: 'flex',
    width: '30%',
    background: props.background ? props.background : '#FFF',
    height: '320px',
    position: 'relative',
    borderRadius: '8px',
    border: `2px solid ${props.borderColor ? props.borderColor : '#EBE5F7'}`,
    padding: '40px',
    flexDirection: 'column',
    gap: '16px',
    [theme.breakpoints.down('lg')]: {
        width: '47%',
        height: 'auto',

    },
    [theme.breakpoints.down('sm')]: {
        width: '95%'
    }
}));

export const CardTitle = styled(Box)(({ theme }) => (props) => ({
    display: 'flex',
    alignItems: 'center',
    gap: '16px'
}));
export const CardTitleImg = styled(Box)(({ theme }) => (props) => ({
    width: '50px',
    height: '50px'
}));
export const CardImg = styled('img')(({ theme }) => (props) => ({
    width: '100%',
    height: '100%'
}));
export const CardTitleText = styled(Typography)(({ theme }) => (props) => ({
    color: props.background !== '#fff' ? theme.palette.primary.light : theme.palette.primary.dark,
    fontSize: "24px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "30px"
}));
export const CardDisc = styled(Typography)(({ theme }) => (props) => ({
    color: props.background !== '#fff' ? theme.palette.primary.light : '#1C103B',
    fontSize: "18px",
    fontStyle: "normal",
    fontWeight: 400,
    lineHeight: "26px"
}));
export const CardDetails = styled(Typography)(({ theme }) => (props) => ({
    color: props.background !== '#fff' ? theme.palette.primary.light : '#1C103B',
    fontSize: "18px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "26px"
}));
export const RoseCardLink = styled(Box)(({ theme }) => (props) => ({
    display: 'flex',
    gap: '12px',
    alignItems: 'center',
    padding: '0',
    width: 'fit-Content',
}));
export const RoseCardLinkFont = styled(Typography)(({ theme }) => (props) => ({
    color: props.background !== '#fff' ? '#FFDA47' : theme.palette.primary.main,
    fontSize: "20px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "30px"
}));
export const RoseCardTitle = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.dark,
    fontSize: "46px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "46px",
    [theme.breakpoints.down('sm')]: {
        fontSize: "32px",
        lineHeight: "32px",

    }
}));





export const ServiceCardBG = styled(Box)(({ theme }) => ({
    position: 'absolute',
    bottom: '0',
    right: '0',
    width: '140px',
    height: '120px',
    backgroundImage: `url("${Images.bgServices}")`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    display: 'flex',
    justifyContent: 'end'
}));
