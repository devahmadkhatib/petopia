import React from 'react'
import * as Styles from './StyledOtherPost';
import Card from '../Shared/CardPost/Card';
import Header from '../Shared/header';
import { dataPost } from '@/features/Home/assets/data/dataPosts';
const OtherPosts = () => {
  return (
      <Styles.PostContainer>
          <Header Title={'Other Posts'} />
          <Styles.PostCards>
              {
                  dataPost.map((card) =>
                      <Card key={card.id} hint={card.hint} title={card.title} user={card.user} disc={card.disc} img={card.img} data={card.date} />
                  )
              }
          </Styles.PostCards>
      </Styles.PostContainer>  )
}

export default OtherPosts