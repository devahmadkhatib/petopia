import React from 'react'
import Header from '../Shared/header';
import * as Styles from './StylrdBlog';
import Card from '../Shared/CardPost/Card';
import { dataBlog } from '@/features/Home/assets/data/dataBlog';
const Blog = () => {
    return (
        <Styles.BlogContainer>
            <Header hint={'OUR BLOG'} Title={'Latest Post'} />
            <Styles.BlogCards>
                {
                    dataBlog.map((card) =>
                        <Card key={card.id} hint={card.hint} title={card.title} user={card.user} disc={card.disc} img={card.img} data={card.date} />
                    )
                }
            </Styles.BlogCards>
        </Styles.BlogContainer>
    )
}

export default Blog