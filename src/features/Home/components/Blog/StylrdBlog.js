import { Box, styled } from "@mui/material";

export const BlogContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'center',
    gap: '32px',
    marginTop: '32px'
}));
export const BlogCards = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    width: '80%',
    justifyContent: 'space-between',
    gap: '16px',
    flexWrap: 'wrap',
    [theme.breakpoints.down('lg')]: {
        width:'95%'
    }
}));
