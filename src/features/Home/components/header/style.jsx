import { Box, styled } from "@mui/material";
import * as Images from '../../assets/images';
export const HeaderBox = styled(Box)(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    backgroundImage: `url("${Images.BackGround}")`,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'right',
    height: 'calc(110vh - 42px)',
    paddingTop: '16px',
    [theme.breakpoints.down('lg')]: {
        height: 'auto',
        backgroundImage: 'none',

    },
    [theme.breakpoints.down('md')]: {
        height: 'auto',
    },
    [theme.breakpoints.down('sm')]: {
        backgroundImage:'none',
        height: 'auto',

    }
}));
export const HeaderContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'space-between',
    flexDirection: 'column',
    width: '80%',
    [theme.breakpoints.down('lg')]: {
        width: '95%',
    }
}));

