import React from 'react'
import *  as Styled from './StyledNavLinks';
import * as Images from '@/core/assets/images';
import * as Comp from '@/core/components';
import * as Icons from '@/core/config/import/icons';
import Links from './Links';
const NavLinks = () => {
    return (
        <Styled.NavLinksContainer>
            <Styled.ContainerSec>
                <Styled.LogoContainer>
                    <Styled.LogoImage>
                        <Styled.Image src={Images.LogoMAin} />
                    </Styled.LogoImage>
                    <Styled.LogoText >
                        Petopia
                    </Styled.LogoText>
                </Styled.LogoContainer>
                <Links />
            </Styled.ContainerSec>
            <Styled.LinksButton>
                <Comp.Button
                    text={<><Icons.ShoppingCartIcon style={{ width: '20px', color: '#7C58D3', marginRight: '4px' }} /> (1 item)  </>}
                    width="120px"
                    height="45px"
                    background="#fff"
                    color="#1C103B"
                    fontSize='16px'
                />
            </Styled.LinksButton>
            <Styled.LinkHum>
                <Icons.DehazeIcon style={{ color: '#B6A0E2' }} />
            </Styled.LinkHum>
        </Styled.NavLinksContainer>
    )
}

export default NavLinks