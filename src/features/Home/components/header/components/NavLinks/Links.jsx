import React from 'react'
import { link } from '@/features/Home/assets/data/Links';
import * as Styled from './StyledNavLinks';
const Links = () => {
    return (
        <Styled.ContainerLinks>
            {
                link.map((Link) =>
                    <Styled.LinksCus style={{ color: Link.color }} to={Link.to} key={Link.id} >
                        {Link.name}
                    </Styled.LinksCus>
                )
            }
        </Styled.ContainerLinks>
    )
}

export default Links