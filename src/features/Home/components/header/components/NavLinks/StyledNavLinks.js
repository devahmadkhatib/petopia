import { Box, IconButton, Typography, styled } from "@mui/material";
import { NavLink } from "react-router-dom";

export const NavLinksContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
}));

export const LogoContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    gap: '16px',
}));
export const LogoText = styled(Typography)(({ theme }) => ({
    color: '#7C58D3',
    fontSize: '24px',
    fontStyle: 'normal',
    fontWeight: 800,
    lineHeight: '18px',
    margin: '0'
}));
export const LogoImage = styled(IconButton)(({ theme }) => ({
    width: '77px',
    padding: '0',
    borderRadius: 'none',
    [theme.breakpoints.down('sm')]: {
        width: '50px',
    }
}));
export const Image = styled('img')(({ theme }) => ({
    width: '100%',
    height: '100%'
}));
export const ContainerSec = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '80%'
}));
export const ContainerLinks = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '60%',
    [theme.breakpoints.down('md')]: {
        display: 'none'
    }
}));
export const LinksCus = styled(NavLink)(({ theme }) => ({
    textDecoration: 'none',
    color: "#392C53",
    fontSize: "18px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "18px"
}));
export const LinksButton = styled(Box)(({ theme }) => ({
    [theme.breakpoints.down('md')]: {
        display: 'none'
    }
}));
export const LinkHum = styled(Box)(({ theme }) => ({
    [theme.breakpoints.up('md')]: {
        display: 'none'
    }
}));


