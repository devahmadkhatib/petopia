import { Box, Typography, styled } from "@mui/material";

export const HeroContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.down('lg')]: {
        flexDirection: 'column-reverse',
        justifyContent: 'center',
        gap: '50px'
    },
    [theme.breakpoints.down('md')]: {

    }
}));
export const HeroLeftSection = styled(Box)(({ theme }) => ({
    display: 'flex',
    gap: '34px',
    flexDirection: 'column',
    width: '45%',
    height: 'calc(94vh - 42px)',
    justifyContent: 'center',
    [theme.breakpoints.down('lg')]: {
        height: 'auto',
        width: '95%',
    },
    [theme.breakpoints.down('md')]: {
        gap: '64px'
    }
}));
export const HeroRightSection = styled(Box)(({ theme }) => ({
    width: '40%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
}));
export const HeroImageDog = styled('img')(({ theme }) => ({
    width: '550px',
    [theme.breakpoints.down('lg')]: {
        width: '400px'
    },
    [theme.breakpoints.down('md')]: {
        width: '350px'
    },
    [theme.breakpoints.down('sm')]: {
        width: '280px'
    },
    [theme.breakpoints.down('ss')]: {
        width: '220px'
    },
}));
export const HeroText = styled(Box)(({ theme }) => ({
    display: 'flex',
    gap: '16px',
    flexDirection: 'column',
}));
export const HeroHint = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.main,
    fontSize: "16px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "19px",
    textTransform: "uppercase",
    [theme.breakpoints.down('sm')]: {
        fontSize: "14px",
        lineHeight: "16px",
    }
}));
export const HeroHeader = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.dark,
    fontSize: "68px",
    fontStyle: "normal",
    fontWeight: 800,
    lineHeight: "71px",
    [theme.breakpoints.down('md')]: {
        fontSize: "48px",
        lineHeight: "50px",
    },
    [theme.breakpoints.down('sm')]: {
        fontSize: "38px",
        lineHeight: "42px",
    },
}));
export const HeroDisc = styled(Typography)(({ theme }) => ({
    color: "#1C103B",
    fontSize: "18px",
    fontStyle: "normal",
    fontWeight: 400,
    lineHeight: "26px",
    width: '85%',
    [theme.breakpoints.down('sm')]: {
        fontSize: "16px",
        lineHeight: "20px",
        width: '95%'
    }
}));
export const HeroSer = styled(Box)(({ theme }) => ({
    width: '97%',
    height: '140px',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
        height: 'auto',
    }
}));
export const HeroCard = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    gap: '16px',
    width: '50%',
    [theme.breakpoints.down('md')]: {
        marginBottom: '8px'
    },
    [theme.breakpoints.down('sm')]: {
        width: '95%',
    }
}));
export const CardText = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '4px'
}));
export const CardBox = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '60px',
    height: '60px',
    borderRadius: "8px",
    border: "2px solid #EBE5F7",
    background: "#FBF9FF"
}));
export const CardBoxImg = styled('img')(({ theme }) => ({

}));
export const CardTextHeader = styled(Typography)(({ theme }) => ({
    color: theme.palette.primary.dark,
    fontSize: "22px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "30px",
    [theme.breakpoints.down('sm')]: {
        fontSize: "18px",
        lineHeight: "22px",
    }
}));
export const CardTextDisc = styled(Typography)(({ theme }) => ({
    color: "#1C103B",
    fontSize: "16px",
    fontFamily: "Lato",
    fontStyle: "normal",
    fontWeight: 400,
    lineHeight: "23px"
}));

