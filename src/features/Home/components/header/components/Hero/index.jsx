import React from 'react'
import Card from './card';
import * as Styled from './styledHero';
import * as Images from '@/features/Home/assets/images';
import { dataSer } from '@/features/Home/assets/data/DataServ';
const Hero = () => {
    return (
        <Styled.HeroContainer>
            <Styled.HeroLeftSection>
                <Styled.HeroText>
                    <Styled.HeroHint>
                        We care for your pets
                    </Styled.HeroHint>
                    <Styled.HeroHeader>
                        We Help You Care for Animals with Nutrition
                    </Styled.HeroHeader>
                    <Styled.HeroDisc>
                        All offers are subject to availability. Ut tortor pretium viverra suspendisse potenti nullam ac tortor vitae. Consectetur a erat nam at. Potenti nullam ac tortor vitae purus faucibus ornare.
                    </Styled.HeroDisc>
                </Styled.HeroText>
                <Styled.HeroSer>
                    {
                        dataSer.map((card) =>
                            <Card key={card.id} title={card.title} disc={card.disc} img={card.img} />
                        )
                    }
                </Styled.HeroSer>
            </Styled.HeroLeftSection>
            <Styled.HeroRightSection>
                <Styled.HeroImageDog src={Images.DogHeader} />
            </Styled.HeroRightSection>
        </Styled.HeroContainer>
    )
}

export default Hero