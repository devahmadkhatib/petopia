import React from 'react'
import * as Styled from './styledHero';
const Card = ({ title, img, disc }) => {
    return (
        <Styled.HeroCard>
            <Styled.CardBox>
                <Styled.CardBoxImg src={img} />
            </Styled.CardBox>
            <Styled.CardText >
                <Styled.CardTextHeader>
                    {title}
                </Styled.CardTextHeader>
                <Styled.CardTextDisc>
                    {disc}
                </Styled.CardTextDisc>
            </Styled.CardText>
        </Styled.HeroCard>
    )
}

export default Card