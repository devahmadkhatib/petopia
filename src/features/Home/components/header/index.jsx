import React from 'react'
import * as Styled from './style';
import * as Comp from './components';
const Header = () => {
    return (
        <Styled.HeaderBox>
            <Styled.HeaderContainer>
                <Comp.NavLinks />
                <Comp.Hero />
            </Styled.HeaderContainer>
        </Styled.HeaderBox>
    )
}

export default Header