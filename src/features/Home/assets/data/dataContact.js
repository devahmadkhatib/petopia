export const dataContact = [
    {
        id: '1',
        title: 'Phone',
        disc1: '(913) 756-3126',
        disc2: '(913) 756-3126',
        color: '#fff',
        backColor: '#7C58D3',
    },
    {
        id: '2',
        title: 'Email',
        disc1: 'petopia@example.com',
        disc2: 'petopia@email.com',
        color: '#7C58D3',
        backColor: '#EBE5F7',

    },
    {
        id: '3',
        title: 'Address',
        disc1: '17 Parkman Place, ',
        disc2: ' 2122United States',
        color: '#7C58D3',
        backColor: '#EBE5F7',

    },
    {
        id: '4',
        title: 'Open Hours',
        disc1: 'Mon - Fri: 7am - 6pm',
        disc2: 'Saturday: 9am - 4pm,',
        color: '#7C58D3',
        backColor: '#EBE5F7',

    },
]