import * as Images from '../images';

export const dataRate = [
    {
        id: '1',
        title: 'Featured Products',
        cards: [
            {
                id: '1',
                img: Images.Dog1,
                title: 'Detachable Gravity Bowl Food Feeder',
                price: '$30.12'
            },
            {
                id: '2',
                img: Images.Dog2,
                title: 'Dog Collar for Small, Medium, Large Dogs',
                price: '$16.88'
            },
            {
                id: '3',
                img: Images.Dog3,
                title: 'Pink Embossed Spiked Collar',
                price: '$34.98'
            },

        ]
    },
    {
        id: '2',
        title: 'On Sale Products',
        cards: [
            {
                id: '1',
                img: Images.Dog2,
                title: 'Black Leather Spike Dog Collar, Small',
                price: '$23.55'
            },
            {
                id: '2',
                img: Images.Dog5,
                title: 'Dog Chew Toys for Chewers',
                price: '$8.99'
            },
            {
                id: '3',
                img: Images.Dog6,
                title: 'Duck Jerky Strips Dog Treats',
                price: '$32.99'
            },

        ]
    },
    {
        id: '3',
        title: 'Top Rated Products',
        cards: [
            {
                id: '1',
                img: Images.Dog3,
                title: 'Carrying Bag for Cats Weighing up to 6 kg',
                price: '$70.43'
            },
            {
                id: '2',
                img: Images.Dog8,
                title: 'Rhinestone Pet Collar',
                price: '$35.66'
            },
            {
                id: '3',
                img: Images.Dog9,
                title: 'Teeth Cleaning Toy for Dogs',
                price: '$12.98'
            },

        ]
    },


]