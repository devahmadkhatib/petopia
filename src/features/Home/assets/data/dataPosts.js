import * as Images from '../images';
export const dataPost = [
    {
        id: '1',
        img: Images.Post1,
        title: '5 Crazy Things Dogs Do When Left Alone At Home',
        disc: 'Est pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat vivamus.',
        hint: 'TRAINING',
        user: 'by Corabelle Durrad',
        date: '02.01.2022'

    },
    {
        id: '2',
        img: Images.Post2,
        title: 'Your Dog Desperately Needs From You ',
        disc: 'Amet porttitor eget dolor morbi non arcu risus quis varius  sodales ut etiam sit amet  ',
        hint: ' TRAINING',
        user: 'by Corabelle Durrad',
        date: '02.01.2022'
    },
    {
        id: '3',
        img: Images.Post3,
        title: 'Top Cat Foods to Consider If You Are a First Time Owner',
        disc: 'Vel eros donec ac odio tempor orci dapibus ultrices. Arcu felis bibendum ut tristique et egestas quis',
        hint: 'PET FOOD',
        user: 'by Corabelle Durrad',
        date: '02.01.2022'
    },
]