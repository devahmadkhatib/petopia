import * as Images from '../images';
export const dataSer = [
    {
        id: '1',
        img: Images.IconSer1,
        title: 'Trust & Safety',
        disc: 'Velit euismod pellentes'
    },
    {
        id: '2',
        img: Images.IconSer2,
        title: 'Discounts ',
        disc: 'Bibendum ut tristique '
    },
    {
        id: '3',
        img: Images.IconSer3,
        title: 'Support',
        disc: 'Egestas quis ipsum velit '
    },
    {
        id: '4',
        img: Images.IconSer4,
        title: 'Guarantee',
        disc: 'Convallis tellus id interdum '
    },
]