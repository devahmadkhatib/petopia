import BackGround from './bg(2).png';
import DogHeader from './dog1 1.png';
import IconSer1 from './shipping-fast.png';
import IconSer2 from './shipping-fast(1).png';
import IconSer3 from './shipping-fast(2).png';
import IconSer4 from './shipping-fast(3).png';
import bgBanaras from './bg(4).png';
import Ban2 from './pet.png';
import Ban1 from './pets.png';
import Ban3 from './canned-food.png';
import Ban4 from './shopping-bag.png';
import DogBan from './pet-accessories-still-life-concept-with-chew-ball.png';
import DogBlue from './pet-dressed-necktie-2021-08-27-22-18-47-utc (1).png';
import DogRose from './charles-deluvio-Mv9hjnEUHR4-unsplash (1).png';
import Dog1 from './2 (1).png';
import Dog2 from './2 (2).png';
import Dog3 from './2 (3).png';
import Dog4 from './2 (4).png';
import Dog5 from './2 (5).png';
import Dog6 from './2 (6).png';
import Dog7 from './2 (7).png';
import Dog8 from './2 (8).png';
import Dog9 from './2 (9).png';
import Blog1 from './crop-groomer-trimming-fur-of-little-dog-2021-11-04-18-19-27-utc(1).png';
import Blog2 from './crop-groomer-trimming-fur-of-little-dog-2021-11-04-18-19-27-utc.png';
import Blog3 from './Img.png';
import Post1 from './dog1 (1).png';
import Post2 from './dog1 (2).png';
import Post3 from './dog1 (3).png';
import bgServices from './Path.png';
import paws from './Paws.png';
export {
    DogHeader,
    IconSer1,
    IconSer2,
    IconSer3,
    IconSer4,
    BackGround,
    bgBanaras,
    Ban2,
    Ban1,
    Ban3,
    Ban4,
    DogBan,
    DogBlue,
    DogRose,
    Dog1,
    Dog2,
    Dog3,
    Dog4,
    Dog5,
    Dog6,
    Dog7,
    Dog8,
    Dog9,
    Blog1,
    Blog2,
    Blog3,
    Post1,
    Post2,
    Post3,
    bgServices,
    paws

}